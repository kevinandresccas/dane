/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.varios.ArchivoLeerTexto;

/**
 *
 * @author MADARME
 */
public class TestArchivoDane {
    
    public static void main(String[] args) {
        String url="https://gitlab.com/madarme/archivos-persistencia/-/raw/master/dptoDane.csv";
        
        //La clase que nos permite leer de una URL:
        ArchivoLeerTexto file=new ArchivoLeerTexto(url);
        Object []v=file.leerTodo();
        for(Object fila:v)
            System.out.println(fila.toString());
    }
    
    
}
